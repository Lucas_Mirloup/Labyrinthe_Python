#-----------------------------------------
# contructeur et accesseurs
#-----------------------------------------

def Matrice(nbLignes,nbColonnes,valeurParDefaut=0):
    """
    crée une matrice de nbLignes lignes sur nbColonnes colonnes en mettant
    valeurParDefaut dans chacune des cases
    paramètres:
      nbLignes un entier strictement positif qui indique le nombre de lignes
      nbColonnes un entier strictement positif qui indique le nombre de colonnes
      valeurParDefaut la valeur par défaut
    résultat la matrice ayant les bonnes propriétés
    """
    matrice=[]
    for i in range(nbLignes):
        ligne=[]
        for j in range(nbColonnes):
            ligne.append(valeurParDefaut)
        matrice.append(ligne)
    return [nbLignes,nbColonnes,matrice]

matrice_assert=Matrice(2,3,1)
assert matrice_assert==[2,3,[[1,1,1],[1,1,1]]]


def getNbLignes(matrice):
    """
    retourne le nombre de lignes de la matrice
    paramètre: matrice la matrice considérée
    """
    return matrice[0]

assert getNbLignes(matrice_assert)==2


def getNbColonnes(matrice):
    """
    retourne le nombre de colonnes de la matrice
    paramètre: matrice la matrice considérée
    """
    return matrice[1]

assert getNbColonnes(matrice_assert)==3


def getVal(matrice,ligne,colonne):
    """
    retourne la valeur qui se trouve en (ligne,colonne) dans la matrice
    paramètres: matrice la matrice considérée
                ligne le numéro de la ligne (en commençant par 0)
                colonne le numéro de la colonne (en commençant par 0)
    """
    if ligne < matrice[0] and colonne < matrice[1]:
        return matrice[2][ligne][colonne]
    else:
        return None

assert getVal(matrice_assert,1,2)==1
assert getVal(matrice_assert,1,3) is None


def setVal(matrice,ligne,colonne,valeur):
    """
    met la valeur dans la case se trouve en (ligne,colonne) de la matrice
    paramètres: matrice la matrice considérée
                ligne le numéro de la ligne (en commençant par 0)
                colonne le numéro de la colonne (en commençant par 0)
                valeur la valeur à stocker dans la matrice
    cette fonction ne retourne rien mais modifie la matrice
    """
    matrice[2][ligne][colonne]=valeur

setVal(matrice_assert,1,1,2)


#------------------------------------------
# decalages
#------------------------------------------
def decalageLigneAGauche(matrice, numLig, nouvelleValeur=0):
    """
    permet de décaler une ligne vers la gauche en insérant une nouvelle
    valeur pour remplacer la premiere case à droite de cette ligne
    le fonction retourne la valeur qui a été éjectée
    paramèteres: matrice la matrice considérée
                 numLig le numéro de la ligne à décaler
                 nouvelleValeur la valeur à placer
    résultat la valeur qui a été ejectée lors du décalage
    """
    res=matrice[2][numLig][0]
    for i in range(matrice[1]-1):
        matrice[2][numLig][i]=matrice[2][numLig][i+1]
    matrice[2][numLig][matrice[1]-1]=nouvelleValeur
    return res

assert decalageLigneAGauche(matrice_assert,1,3)==1


def decalageLigneADroite(matrice, numLig, nouvelleValeur=0):
    """
    decale la ligne numLig d'une case vers la droite en insérant une nouvelle
    valeur pour remplacer la premiere case à gauche de cette ligne
    paramèteres: matrice la matrice considérée
                 numLig le numéro de la ligne à décaler
                 nouvelleValeur la valeur à placer
    résultat: la valeur de la case "ejectée" par le décalage
    """
    res=matrice[2][numLig][matrice[1]-1]
    liste=range(matrice[1]-1,0,-1)
    for i in liste:
        matrice[2][numLig][i]=matrice[2][numLig][i-1]
    matrice[2][numLig][0]=nouvelleValeur
    return res

assert decalageLigneADroite(matrice_assert,1,0)==3


def decalageColonneEnHaut(matrice, numCol, nouvelleValeur=0):
    """
    decale la colonne numCol d'une case vers le haut en insérant une nouvelle
    valeur pour remplacer la premiere case en bas de cette ligne
    paramèteres: matrice la matrice considérée
                 numCol le numéro de la colonne à décaler
                 nouvelleValeur la valeur à placer
    résultat: la valeur de la case "ejectée" par le décalage
    """
    res=matrice[2][0][numCol]
    for i in range(matrice[0]-1):
        matrice[2][i][numCol]=matrice[2][i+1][numCol]
    matrice[2][matrice[0]-1][numCol]=nouvelleValeur
    return res

assert decalageColonneEnHaut(matrice_assert,1,0)==1


def decalageColonneEnBas(matrice, numCol, nouvelleValeur=0):
    """
    decale la colonne numCol d'une case vers le bas en insérant une nouvelle
    valeur pour remplacer la premiere case en haut de cette ligne
    paramèteres: matrice la matrice considérée
                 numCol le numéro de la colonne à décaler
                 nouvelleValeur la valeur à placer
    résultat: la valeur de la case "ejectée" par le décalage
    """
    res=matrice[2][matrice[0]-1][numCol]
    for i in range(matrice[0]-1,0,-1):
        matrice[2][i][numCol]=matrice[2][i-1][numCol]
    matrice[2][0][numCol]=nouvelleValeur
    return res

assert decalageColonneEnBas(matrice_assert,1,0)==0
