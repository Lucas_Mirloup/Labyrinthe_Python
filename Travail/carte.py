import random

"""
la liste des caractères semi-graphiques correspondant aux différentes cartes
l'indice du caractère dans la liste correspond au codage des murs sur la carte
le caractère 'Ø' indique que l'indice ne correspond pas à une carte
"""
listeCartes = ['╬','╦','╣','╗','╩','═','╝','Ø','╠','╔','║','Ø','╚','Ø','Ø','Ø']

# Exemples utilisés dans les tests
exemple1 = {"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]}
exemple2 = {"N":True,"E":False,"S":False,"O":False,"tresor":1,"pions":[2,3]}
exemple3 = {"N":True,"E":False,"S":False,"O":True,"tresor":0,"pions":[]}

def Carte( nord=False, est=False, sud=False, ouest=False, tresor=0, pions=[]):

    """
    permet de créer une carte:
    paramètres:
    nord, est, sud et ouest sont des booléens indiquant s'il y a un mur ou non dans chaque direction
    tresor est le numéro du trésor qui se trouve sur la carte (0 s'il n'y a pas de trésor)
    pions est la liste des pions qui sont posés sur la carte (un pion est un entier entre 1 et 4)
    """
    c={"N":nord,"E":est,"S":sud,"O":ouest,"tresor":tresor,"pions":pions}
    return c

assert Carte(False,False,True,True) == {"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]}


def estValide(c):
    """
    retourne un booléen indiquant si la carte est valide ou non c'est à dire qu'elle a un ou deux murs
    paramètre: c une carte
    """
    cpt=0
    for sens in {"N","E","S","O"}:
        if c[sens]:
            cpt=cpt+1
            if cpt==2:
                return True
    return False

assert estValide({"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]})
assert not estValide({"N":True,"E":False,"S":False,"O":False,"tresor":1,"pions":[2,3]})


def murNord(c):
    """
    retourne un booléen indiquant si la carte possède un mur au nord
    paramètre: c une carte
    """
    return c["N"]

assert not murNord({"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]})
assert murNord({"N":True,"E":False,"S":False,"O":False,"tresor":1,"pions":[2,3]})


def murSud(c):
    """
    retourne un booléen indiquant si la carte possède un mur au sud
    paramètre: c une carte
    """
    return c["S"]

assert murSud({"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]})
assert not murSud({"N":True,"E":False,"S":False,"O":False,"tresor":1,"pions":[2,3]})


def murEst(c):
    """
    retourne un booléen indiquant si la carte possède un mur à l'est
    paramètre: c une carte
    """
    return c["E"]

assert not murEst({"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]})
assert murEst({"N":False,"E":True,"S":False,"O":False,"tresor":1,"pions":[2,3]})


def murOuest(c):
    """
    retourne un booléen indiquant si la carte possède un mur à l'ouest
    paramètre: c une carte
    """
    return c["O"]

assert murOuest({"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]})
assert not murOuest({"N":True,"E":False,"S":False,"O":False,"tresor":1,"pions":[2,3]})


def getListePions(c):
    """
    retourne la liste des pions se trouvant sur la carte
    paramètre: c une carte
    """
    return c["pions"]

assert getListePions({"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]}) == []
assert getListePions({"N":True,"E":False,"S":False,"O":False,"tresor":1,"pions":[2,3]}) == [2,3]


def setListePions(c,listePions):
    """
    place la liste des pions passées en paramètre sur la carte
    paramètres: c: est une carte
                listePions: la liste des pions à poser
    Cette fonction ne retourne rien mais modifie la carte
    """
    c["pions"]=listePions

setListePions(exemple1,[2,3])
assert exemple1=={"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[2,3]}


def getNbPions(c):
    """
    retourne le nombre de pions se trouvant sur la carte
    paramètre: c une carte
    """
    return len(c["pions"])

assert getNbPions({"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]}) == 0
assert getNbPions({"N":True,"E":False,"S":False,"O":False,"tresor":1,"pions":[2,3]}) == 2


def possedePion(c,pion):
    """
    retourne un booléen indiquant si la carte possède le pion passé en paramètre
    paramètre: c une carte
    """
    for joueur in c["pions"]:
        if joueur == pion:
            return True
    return False

assert not possedePion({"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]},1)
assert possedePion({"N":True,"E":False,"S":False,"O":False,"tresor":1,"pions":[2,3]},2)


def getTresor(c):
    """
    retourne la valeur du trésor qui se trouve sur la carte (0 si pas de trésor)
    paramètre: c une carte
    """
    return c["tresor"]

assert getTresor({"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]}) == 0
assert getTresor({"N":False,"E":False,"S":True,"O":True,"tresor":2,"pions":[]}) == 2


def prendreTresor(c):
    """
    enlève le trésor qui se trouve sur la carte et retourne la valeur de ce trésor
    paramètre: c une carte
    Cette fonction modifie la carte mais ne retourne rien
    """
    res=c["tresor"]
    c["tresor"]=0
    return res

assert prendreTresor({"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]}) == 0
assert prendreTresor({"N":False,"E":False,"S":True,"O":True,"tresor":2,"pions":[]}) == 2


def mettreTresor(c,tresor):
    """
    met le trésor passé en paramètre sur la carte et retourne la valeur de l'ancien trésor
    paramètres: c une carte
                tresor un entier positif
    Cette fonction modifie la carte mais ne retourne rien
    """
    res=c["tresor"]
    c["tresor"]=tresor
    return res

assert mettreTresor({"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]},2) == 0
assert mettreTresor({"N":False,"E":False,"S":True,"O":True,"tresor":2,"pions":[]},3) == 2


def prendrePion(c, pion):
    """
    enlève le pion passé en paramètre de la carte. Si le pion n'y était pas ne fait rien
    paramètres: c une carte
                pion un entier compris entre 1 et 4
    Cette fonction modifie la carte mais ne retourne rien
    """
    if pion in c["pions"]:
        res=[]
        for elem in c["pions"]:
            if elem != pion:
                res.append(elem)
        c["pions"]=res

prendrePion(exemple2,2)
assert exemple2=={"N":True,"E":False,"S":False,"O":False,"tresor":1,"pions":[3]}


def poserPion(c, pion):
    """
    pose le pion passé en paramètre sur la carte. Si le pion y était déjà ne fait rien
    paramètres: c une carte
                pion un entier compris entre 1 et 4
    Cette fonction modifie la carte mais ne retourne rien
    """
    if pion not in c["pions"]:
        c["pions"].append(pion)
    return c

assert poserPion({"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]},1) == {"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[1]}
assert poserPion({"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[1]},1) == {"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[1]}

def tournerHoraire(c):
    """
    fait tourner la carte dans le sens horaire
    paramètres: c une carte
    Cette fonction modifie la carte mais ne retourne rien
    """
    nord=c["O"]
    est=c["N"]
    sud=c["E"]
    ouest=c["S"]
    c["N"]=nord
    c["E"]=est
    c["S"]=sud
    c["O"]=ouest

tournerHoraire(exemple3)
assert exemple3 == {"N":True,"E":True,"S":False,"O":False,"tresor":0,"pions":[]}


def tournerAntiHoraire(c):
    """
    fait tourner la carte dans le sens anti-horaire
    paramètres: c une carte
    Cette fonction modifie la carte mais ne retourne rien
    """
    nord=c["E"]
    est=c["S"]
    sud=c["O"]
    ouest=c["N"]
    c["N"]=nord
    c["E"]=est
    c["S"]=sud
    c["O"]=ouest


tournerAntiHoraire(exemple3)
assert exemple3 =={"N":True,"E":False,"S":False,"O":True,"tresor":0,"pions":[]}


def tourneAleatoire(c):
    """
    faire tourner la carte d'un nombre de tours aléatoire
    paramètres: c une carte
    Cette fonction modifie la carte mais ne retourne rien
    """
    sens = random.randint(0,3)
    for i in range(0,sens):
        tournerHoraire(c)


def coderMurs(c):
    """
    code les murs sous la forme d'un entier dont le codage binaire
    est de la forme bNbEbSbO où bN, bE, bS et bO valent
       soit 0 s'il n'y a pas de mur dans dans la direction correspondante
       soit 1 s'il y a un mur dans la direction correspondante
    bN est le chiffre des unité, BE des dizaine, etc...
    le code obtenu permet d'obtenir l'indice du caractère semi-graphique
    correspondant à la carte dans la liste listeCartes au début de ce fichier
    paramètre c une carte
    retourne un entier indice du caractère semi-graphique de la carte
    """
    res=0
    if murNord(c):
        res += 1
    if murEst(c):
        res += 2
    if murSud(c):
        res += 4
    if murOuest(c):
        res += 8
    return res

assert coderMurs({"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]})==12


def decoderMurs(c,code):
    """
    positionne les murs d'une carte en fonction du code décrit précédemment
    paramètres c une carte
               code un entier codant les murs d'une carte
    Cette fonction modifie la carte mais ne retourne rien
    """
    if code - 8 >= 0:
        code -= 8
        c["O"] = True
    else:
        c["O"] = False
    if code - 4 >= 0:
        code -= 4
        c["S"] = True
    else:
        c["S"] = False
    if code - 2 >= 0:
        code -= 2
        c["E"] = True
    else:
        c["E"] = False
    if code - 1 >= 0:
        c["N"] = True
    else:
        c["N"] = False
    return c

carte={"N":True,"E":True,"S":True,"O":True,"tresor":0,"pions":[]}
decoderMurs(carte,12)
assert carte=={'O': True, 'N': False, 'E': False, 'S': True, 'tresor': 0, 'pions': []}


def toChar(c):
    """
    fournit le caractère semi graphique correspondant à la carte (voir la variable listeCartes au début de ce script)
    paramètres c une carte
    """
    return listeCartes[coderMurs(c)]

assert toChar(carte)=='╚'


def passageNord(carte1,carte2):
    """
    suppose que la carte2 est placée au nord de la carte1 et indique
    s'il y a un passage entre ces deux cartes en passant par le nord
    paramètres carte1 et carte2 deux carte
    résultat un booléen
    """
    return not murNord(carte1) and not murSud(carte2)
    #return carte1["N"] and carte2["S"]

assert passageNord(exemple1,exemple2)

exemple1 = {"N":False,"E":False,"S":True,"O":True,"tresor":0,"pions":[]}
exemple2 = {"N":True,"E":False,"S":False,"O":False,"tresor":1,"pions":[2,3]}


def passageSud(carte1,carte2):
    """
    suppose que la carte2 est placée au sud de la carte1 et indique
    s'il y a un passage entre ces deux cartes en passant par le sud
    paramètres carte1 et carte2 deux carte
    résultat un booléen
    """
    return not murSud(carte1) and not murNord(carte2)

assert not passageSud(exemple1,exemple2)


def passageOuest(carte1,carte2):
    """
    suppose que la carte2 est placée à l'ouest de la carte1 et indique
    s'il y a un passage entre ces deux cartes en passant par l'ouest
    paramètres carte1 et carte2 deux carte
    résultat un booléen
    """
    return not murOuest(carte1) and not murEst(carte2)

assert not passageOuest(exemple1,exemple2)


def passageEst(carte1,carte2):
    """
    suppose que la carte2 est placée à l'est de la carte1 et indique
    s'il y a un passage entre ces deux cartes en passant par l'est
    paramètres carte1 et carte2 deux carte
    résultat un booléen
    """
    return not murEst(carte1) and not murOuest(carte2)

assert passageEst(exemple1,exemple2)
