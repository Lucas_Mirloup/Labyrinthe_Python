from matrice import *
from carte import *

#def affiche(matrice):
#    listeCartes = ['╬','╦','╣','╗','╩','═','╝','Ø','╠','╔','║','Ø','╚','Ø','Ø','Ø']
#    for i in range(matrice[0]):
#        ligne = []
#        for j in range(matrice[1]):
#            #print(matrice[2][i][j])
#            ligne.append(listeCartes[coderMurs(matrice[2][i][j])])
#        print(ligne)


def Plateau(nbJoueurs, nbTresors):
    """
    créer un nouveau plateau contenant nbJoueurs et nbTrésors
    paramètres: nbJoueurs le nombre de joueurs (un nombre entre 1 et 4)
                nbTresors le nombre de trésor à placer (un nombre entre 12 et 45)
    resultat: un couple contenant
              - une matrice de taille 7x7 représentant un plateau de labyrinthe où les cartes
                ont été placée de manière aléatoire
              - la carte amovible qui n'a pas été placée sur le plateau
    """
    matrice=Matrice(7,7,{"N":False,"E":False,"S":False,"O":False,"tresor":0,"pions":[]})
    creerCartesPlateau(matrice)
    PoserJoueurDepart(nbJoueurs,matrice)
    PoserTresorDepart(nbTresors, matrice)
    amovible=Carte()
    code=random.choice([0,1,2,3,4,5,6,8,9,10,12])
    decoderMurs(amovible,code)
    carteAmovible=[amovible,(0,0)]
    #affiche(matrice)
    return [matrice,carteAmovible]

def creerCartesPlateau(matrice):
    """
    pour chaque case de la matrice lui associe une carte fixe ou aléatoire selon sa postion
    paramètre: matrice est la matrice du plateau de jeu
    Cette fonction modifie la matrice mais ne retourne rien
    """
    fixes=[9,1,1,3,8,8,1,2,8,4,2,2,12,4,4,6]
    restantes=[16,6,12]
    angles=[3,6,9,12]
    jonctions=[1,2,4,8]
    droits=[5,10]
    aleatoires=[angles,jonctions,droits]
    cpt_fixe=0
    for i in range(0,7):
        for j in range(0,7):
            if i%2==0 and j%2==0:
                matrice[2][i][j]=decoderMurs({"N":False,"E":False,"S":False,"O":False,"tresor":0,"pions":[]},fixes[cpt_fixe])
                cpt_fixe += 1
            else:
                ok = False
                while not ok:
                    ri=random.randint(0,2)
                    if restantes[ri]>0:
                        code=random.choice(aleatoires[ri])
                        matrice[2][i][j]=decoderMurs({"N":False,"E":False,"S":False,"O":False,"tresor":0,"pions":[]},code)
                        restantes[ri]-=1
                        ok = True


def PoserJoueurDepart(nbJoueurs, matrice):
    """
    pose les pions de chaque joueur sur les carte correspondante à leur position de départ
    paramètre: matrice est la matrice du plateau de jeu
               nbJoueurs le nombre de joueurs (un nombre entre 1 et 4)
    Cette fonction modifie la matrice mais ne retourne rien
    """
    setListePions(matrice[2][0][0],[1])
    if nbJoueurs>=2:
        setListePions(matrice[2][0][6],[2])
    if nbJoueurs>=3:
        setListePions(matrice[2][6][0],[3])
    if nbJoueurs==4:
        setListePions(matrice[2][6][6],[4])


def PoserTresorDepart(nbTresors, matrice):
    """
    pose les trésor sur des cases aléatoire et sur toute les cases fixes hors case de départ
    paramètre: matrice est la matrice du plateau de jeu
               nbTresors le nombre de trésor à placer (un nombre entre 12 et 45)
    Cette fonction modifie la matrice mais ne retourne rien
    """
    #il y a forcement 24 trésor au total, 12 tresor sur carte fixe hors angle et 12 trésor sur carte aléatoire
    ensemble_tresor=set(range(1,nbTresors+1))
    nbTresorsPlace=0
    while nbTresorsPlace<nbTresors:
        ligne=0
        colonne=0
        while (ligne,colonne)==(0,0) or (ligne,colonne)==(0,6) or (ligne,colonne)==(6,0) or (ligne,colonne)==(6,6):
            ligne = random.randint(0,6)
            colonne = random.randint(0,6)
        if ligne%2==0 and colonne%2==0 and nbTresorsPlace<12 and getTresor(matrice[2][ligne][colonne]) == 0:
            tresor=random.choice(list(ensemble_tresor))
            ensemble_tresor = ensemble_tresor - {tresor}
            mettreTresor(matrice[2][ligne][colonne],tresor)
            nbTresorsPlace += 1
        elif nbTresorsPlace >= 12  and getTresor(matrice[2][ligne][colonne]) == 0:
            tresor=random.choice(list(ensemble_tresor))
            ensemble_tresor = ensemble_tresor - {tresor}
            mettreTresor(matrice[2][ligne][colonne],tresor)
            nbTresorsPlace += 1


#Plateau(4,24)
#affiche(Plateau(4,24)[0])

def creerCartesAmovibles(tresorDebut, nbTresors):
    """
    fonction utilitaire qui permet de créer les cartes amovibles du jeu en y positionnant
    aléatoirement nbTresor trésors
    la fonction retourne la liste, mélangée aléatoirement, des cartes ainsi créées
    paramètres: tresorDebut: le numéro du premier trésor à créer
                nbTresors: le nombre total de trésor à créer
    résultat: la liste mélangée aléatoirement des cartes amovibles créees
    """
    pass

def prendreTresorPlateau(plateau,lig,col,numTresor):
    """
    prend le tresor numTresor qui se trouve sur la carte en lin,col du plateau
    retourne True si l'opération s'est bien passée (le trésor était vraiment sur
    la carte
    paramètres: plateau: le plateau considéré
                lig: la ligne où se trouve la carte
                col: la colonne où se trouve la carte
                numTresor: le numéro du trésor à prendre sur la carte
    resultat: un booléen indiquant si le trésor était bien sur la carte considérée
    """
    if getTresor(plateau[2][lig][col]) == numTresor:
        if prendreTresor(plateau[2][lig][col]):
            return True
    return False


def getCoordonneesTresor(plateau,numTresor):
    """
    retourne les coordonnées sous la forme (lig,col) du trésor passé en paramètre
    paramètres: plateau: le plateau considéré
                numTresor: le numéro du trésor à trouver
    resultat: un couple d'entier donnant les coordonnées du trésor ou None si
              le trésor n'est pas sur le plateau
    """
    for i in range(plateau[0]):
        for j in range(plateau[1]):
            if plateau[2][i][j]["tresor"]==numTresor:
                return (i,j)
    return None


def getCoordonneesJoueur(plateau,numJoueur):
    """
    retourne les coordonnées sous la forme (lig,col) du joueur passé en paramètre
    paramètres: plateau: le plateau considéré
                numJoueur: le numéro du joueur à trouver
    resultat: un couple d'entier donnant les coordonnées du joueur ou None si
              le joueur n'est pas sur le plateau
    """
    for i in range(getNbLignes(plateau)):
        for j in range(getNbColonnes(plateau)):
            if numJoueur in plateau[2][i][j]["pions"]:
                return (i,j)
    return None

def prendrePionPlateau(plateau,lin,col,numJoueur):
    """
    prend le pion du joueur sur la carte qui se trouve en (lig,col) du plateau
    paramètres: plateau:le plateau considéré
                lin: numéro de la ligne où se trouve le pion
                col: numéro de la colonne où se trouve le pion
                numJoueur: le numéro du joueur qui correspond au pion
    Cette fonction ne retourne rien mais elle modifie le plateau
    """
    prendrePion(plateau[2][lin][col], numJoueur)


def poserPionPlateau(plateau,lin,col,numJoueur):
    """
    met le pion du joueur sur la carte qui se trouve en (lig,col) du plateau
    paramètres: plateau:le plateau considéré
                lin: numéro de la ligne où se trouve le pion
                col: numéro de la colonne où se trouve le pion
                numJoueur: le numéro du joueur qui correspond au pion
    Cette fonction ne retourne rien mais elle modifie le plateau
    """
    poserPion(plateau[2][lin][col], numJoueur)

def creerCalque(matrice):
    """
    créer un calque
    paramètre: matrice: est la matrice du plateau de jeu
    Cette fonction retourne un calque
    """
    nbLig = getNbLignes(matrice)
    nbCol = getNbColonnes(matrice)
    calque = Matrice(nbLig,nbCol)
    return calque


def marquageDirect(matrice,calque,val,marque):
    """
    marque les case du calque auquel on peut accéder depuis les case avec la valeur val
    paramètre: matrice: est la matrice du plateau de jeu
               calque: est une matrice utiliser pour voir les chemin possible dans matrice
               val: valeur des cartes de départs
               marque: valeur a mettre sur les carte accessible depuis les carte de départs
    Cette fonction modifie le calque mais ne retourne rien
    """
    suite=False
    for i in range(0,7):
        for j in range(0,7):
            if getVal(calque,i,j) == val:
                if j<6:
                    carte1=matrice[2][i][j]
                    carte2=matrice[2][i][j+1]
                    if passageEst(carte1,carte2):
                        if getVal(calque,i,j+1)==0:
                            setVal(calque,i,j+1,marque)
                            suite=True
                if j>0:
                    carte1=matrice[2][i][j]
                    carte2=matrice[2][i][j-1]
                    if passageOuest(carte1,carte2):
                        if getVal(calque,i,j-1)==0:
                            setVal(calque,i,j-1,marque)
                            suite=True
                if i<6:
                    carte1=matrice[2][i][j]
                    carte2=matrice[2][i+1][j]
                    if passageSud(carte1,carte2):
                        if getVal(calque,i+1,j)==0:
                            setVal(calque,i+1,j,marque)
                            suite=True
                if i>0:
                    carte1=matrice[2][i][j]
                    carte2=matrice[2][i-1][j]
                    if passageNord(carte1,carte2):
                        if getVal(calque,i-1,j)==0:
                            setVal(calque,i-1,j,marque)
                            suite=True
    return suite



def accessible(plateau,ligD,colD,ligA,colA):
    global calque
    """
    indique si il y a un chemin entre la case ligD,colD et la case ligA,colA du labyrinthe
    paramètres: plateau: le plateau considéré
                ligD: la ligne de la case de départ
                colD: la colonne de la case de départ
                ligA: la ligne de la case d'arrivée
                colA: la colonne de la case d'arrivée
    résultat: un boolean indiquant s'il existe un chemin entre la case de départ
              et la case d'arrivée
    """
    calque = creerCalque(plateau)
    ok=False
    setVal(calque,ligD,colD,1)
    #print(calque)
    i=1
    while not ok:
        ok=not marquageDirect(plateau,calque,i,i+1)
        #print(calque)
        i += 1
        if not ok and getVal(calque,ligA,colA)!=0 and getVal(calque,ligD,colD)!=0 :
            return True

    return False

#print(accessible(Plateau(4,24),0,0,1,1))

def cheminPlusCourt(matrice,calque,ligD,colD,ligA,colA):
    """
    cette fonction renvoi la liste des coordonnée permettant d'arriver de la case d'arriver à la case de départ
    paramètres: matrice: la matrice du plateau
                calque: une matrice utiliser pour voir les chemin possible dans matrice
                ligD: la ligne de la case de départ
                colD: la colonne de la case de départ
                ligA: la ligne de la case d'arrivée
                colA: la colonne de la case d'arrivée
    retourne une liste de cooerdonnée
    """
    res=[(ligA,colA)]
    valArrive=getVal(calque,ligA,colA)
    val=valArrive
    while val>1:
        ligne=res[-1][0]
        colonne=res[-1][1]
        coordonneAjoute=False
        if colonne<6 and not coordonneAjoute:
            if getVal(calque,ligne,colonne+1) == val-1:
                carte1=matrice[2][ligne][colonne]
                carte2=matrice[2][ligne][colonne+1]
                if passageEst(carte1,carte2):
                    res.append((ligne,colonne+1))
                    coordonneAjoute=True
                    val -= 1
        if colonne>0 and not coordonneAjoute:
            if getVal(calque,ligne,colonne-1) == val-1:
                carte1=matrice[2][ligne][colonne]
                carte2=matrice[2][ligne][colonne-1]
                if passageOuest(carte1,carte2):
                    res.append((ligne,colonne-1))
                    coordonneAjoute=True
                    val -= 1
        if ligne<6 and not coordonneAjoute:
            if getVal(calque,ligne+1,colonne) == val-1:
                carte1=matrice[2][ligne][colonne]
                carte2=matrice[2][ligne+1][colonne]
                if passageSud(carte1,carte2):
                    res.append((ligne+1,colonne))
                    coordonneAjoute=True
                    val -= 1
        if ligne>0 and not coordonneAjoute:
            if getVal(calque,ligne-1,colonne) == val-1:
                carte1=matrice[2][ligne][colonne]
                carte2=matrice[2][ligne-1][colonne]
                if passageNord(carte1,carte2):
                    res.append((ligne-1,colonne))
                    coordonneAjoute=True
                    val -= 1
    return res




def accessibleDist(plateau,ligD,colD,ligA,colA):
    """
    indique si il y a un chemin entre la case ligD,colD et la case ligA,colA du plateau
    mais la valeur de retour est None s'il n'y a pas de chemin,
    sinon c'est un chemin possible entre ces deux cases sous la forme d'une liste
    de coordonées (couple de (lig,col))
    paramètres: plateau: le plateau considéré
                ligD: la ligne de la case de départ
                colD: la colonne de la case de départ
                ligA: la ligne de la case d'arrivée
                colA: la colonne de la case d'arrivée
    résultat: une liste de coordonées indiquant un chemin possible entre la case
              de départ et la case d'arrivée
    """
    #print(plateau)
    if accessible(plateau,ligD,colD,ligA,colA):
        res=cheminPlusCourt(plateau,calque,ligD,colD,ligA,colA)
        res.reverse()
        return res
    else:
        return None


accessibleDist(Plateau(4,24)[0],0,0,1,1)
