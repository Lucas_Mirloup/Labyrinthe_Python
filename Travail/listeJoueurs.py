import random
from joueur import *

joueurs_assert=["Julien","Baptiste"]

def ListeJoueurs(nomsJoueurs):
    """
    créer une liste de joueurs dont les noms sont dans la liste de noms passés en paramètre
    Attention il s'agit d'une liste de joueurs qui gère la notion de joueur courant
    paramètre: nomsJoueurs une liste de chaines de caractères
    résultat: la liste des joueurs avec un joueur courant mis à 0
    """
    res=[0,[]]
    for nomJoueur in nomsJoueurs:
        res[1].append(Joueur(nomJoueur))
    return res

joueurs_assert=ListeJoueurs(joueurs_assert)
assert joueurs_assert == [0, [{'nom': 'Julien', 'TresorATrouver': []}, {'nom': 'Baptiste', 'TresorATrouver': []}]]


def ajouterJoueur(joueurs,joueur):
    """
    ajoute un nouveau joueur à la fin de la liste
    paramètres: joueurs un liste de joueurs
                joueur le joueur à ajouter
    cette fonction ne retourne rien mais modifie la liste des joueurs
    """
    joueurs[1].append(joueur)

ajouterJoueur(joueurs_assert,Joueur("Lucas"))
assert joueurs_assert==[0, [{'nom': 'Julien', 'TresorATrouver': []}, {'nom': 'Baptiste', 'TresorATrouver': []}, {'nom': 'Lucas', 'TresorATrouver': []}]]


def initAleatoireJoueurCourant(joueurs):
    """
    tire au sort le joueur courant
    paramètre: joueurs un liste de joueurs
    cette fonction ne retourne rien mais modifie la liste des joueurs
    """
    IndiceJouCou=random.randint(0,len(joueurs[1])-1)
    joueurs[0]=IndiceJouCou

initAleatoireJoueurCourant(joueurs_assert)


def distribuerTresors(joueurs,nbTresors=24, nbTresorsMax=0):
    """
    distribue de manière aléatoire des trésors entre les joueurs.
    paramètres: joueurs la liste des joueurs
                nbTresors le nombre total de trésors à distribuer (on rappelle
                        que les trésors sont des entiers de 1 à nbTresors)
                nbTresorsMax un entier fixant le nombre maximum de trésor
                             qu'un joueur aura après la distribution
                             si ce paramètre vaut 0 on distribue le maximum
                             de trésor possible
    cette fonction ne retourne rien mais modifie la liste des joueurs
    """
    listeTresor=[]
    for i in range(1,nbTresors+1):
        listeTresor.append(i)
    if nbTresorsMax==0:
        for joueur in joueurs[1]:
            for _ in range(nbTresors//len(joueurs[1])):
                tresor=random.choice(listeTresor)
                ajouterTresor(joueur,tresor)
                listeTresor.remove(tresor)
    else:
        for joueur in joueurs[1]:
            for _ in range(nbTresorsMax):
                tresor=random.choice(listeTresor)
                ajouterTresor(joueur,tresor)
                listeTresor.remove(tresor)

distribuerTresors(joueurs_assert,24,3)

joueurs_assert2=[0, [{'nom': 'Julien', 'TresorATrouver': [1,2]}, {'nom': 'Baptiste', 'TresorATrouver': [3,4]}]]

def changerJoueurCourant(joueurs):
    """
    passe au joueur suivant (change le joueur courant donc)
    paramètres: joueurs la liste des joueurs
    cette fonction ne retourne rien mais modifie la liste des joueurs
    """
    if joueurs[0]+1==len(joueurs[1]):
        joueurs[0]=0
    else:
        joueurs[0]+=1

changerJoueurCourant(joueurs_assert2)
assert joueurs_assert2==[1, [{'nom': 'Julien', 'TresorATrouver': [1,2]}, {'nom': 'Baptiste', 'TresorATrouver': [3,4]}]]

def getNbJoueurs(joueurs):
    """
    retourne le nombre de joueurs participant à la partie
    paramètre: joueurs la liste des joueurs
    résultat: le nombre de joueurs de la partie
    """
    return len(joueurs[1])


def getJoueurCourant(joueurs):
    """
    retourne le joueur courant
    paramètre: joueurs la liste des joueurs
    cette fonction ne retourne rien mais modifie la liste des joueurs
    """
    #print(joueurs)
    return joueurs[1][joueurs[0]]

assert getJoueurCourant(joueurs_assert2)=={'nom': 'Baptiste', 'TresorATrouver': [3,4]}

def joueurCourantTrouveTresor(joueurs):
    """
    Met à jour le joueur courant lorsqu'il a trouvé un trésor
    c-à-d enlève le trésor de sa liste de trésors à trouver
    paramètre: joueurs la liste des joueurs
    cette fonction ne retourne rien mais modifie la liste des joueurs
    """
    tresorTrouve(getJoueurCourant(joueurs))

joueurCourantTrouveTresor(joueurs_assert2)
assert joueurs_assert2==[1, [{'nom': 'Julien', 'TresorATrouver': [1,2]}, {'nom': 'Baptiste', 'TresorATrouver': [4]}]]


def nbTresorsRestantsJoueur(joueurs,numJoueur):
    """
    retourne le nombre de trésor restant pour le joueur dont le numero
    est donné en paramètre
    paramètres: joueurs la liste des joueurs
                numJoueur le numéro du joueur
    résultat: le nombre de trésors que joueur numJoueur doit encore trouver
    """
    return len(joueurs[1][numJoueur-1]["TresorATrouver"])

assert nbTresorsRestantsJoueur(joueurs_assert2,2)==1


def numJoueurCourant(joueurs):
    """
    retourne le numéro du joueur courant
    paramètre: joueurs la liste des joueurs
    résultat: le numéro du joueur courant
    """
    return joueurs[0]+1



assert numJoueurCourant(joueurs_assert2)==2

#joueurs_assert2=[0, [{'nom': 'Julien', 'TresorATrouver': [1,2]}, {'nom': 'Baptiste', 'TresorATrouver': [3,4]}]]
def nomJoueurCourant(joueurs):
    """
    retourne le nom du joueur courant
    paramètre: joueurs la liste des joueurs
    résultat: le nom du joueur courant
    """
    #print(joueurs)
    return  getNom(getJoueurCourant(joueurs))

assert nomJoueurCourant(joueurs_assert2)=="Baptiste"


def nomJoueur(joueurs,numJoueur):
    """
    retourne le nom du joueur dont le numero est donné en paramètre
    paramètres: joueurs la liste des joueurs
                numJoueur le numéro du joueur
    résultat: le nom du joueur numJoueur
    """
    return joueurs[1][numJoueur-1]["nom"]

assert nomJoueur(joueurs_assert2,1)=="Julien"


def prochainTresorJoueur(joueurs,numJoueur):
    """
    retourne le trésor courant du joueur dont le numero est donné en paramètre
    paramètres: joueurs la liste des joueurs
                numJoueur le numéro du joueur
    résultat: le prochain trésor du joueur numJoueur (un entier)
    """
    return prochainTresor(joueurs[1][numJoueur-1])

assert prochainTresorJoueur(joueurs_assert2,1)==1


def tresorCourant(joueurs):
    """
    retourne le trésor courant du joueur courant
    paramètre: joueurs la liste des joueurs
    résultat: le prochain trésor du joueur courant (un entier)
    """
    return prochainTresor(getJoueurCourant(joueurs))

assert tresorCourant(joueurs_assert2)==4


def joueurCourantAFini(joueurs):
    """
    indique si le joueur courant a gagné
    paramètre: joueurs la liste des joueurs
    résultat: un booleen indiquant si le joueur courant a fini
    """
    res=False
    if len(joueurs[1][joueurs[0]]["TresorATrouver"])==0:
        res=True
    return res

assert not joueurCourantAFini(joueurs_assert2)
