from listeJoueurs import *
from plateau import *


def Labyrinthe(nomsJoueurs=["joueur1","joueur2"],nbTresors=24, nbTresorsMax=0):
    """
    permet de créer un labyrinthe avec nbJoueurs joueurs, nbTresors trésors
    chacun des joueurs aura au plus nbTresorMax à trouver
    si ce dernier paramètre est à 0, on distribuera le maximum de trésors possible
    à chaque joueur en restant équitable
    un joueur courant est choisi et la phase est initialisée
    paramètres: nomsJoueurs est la liste des noms des joueurs participant à la partie (entre 1 et 4)
                nbTresors le nombre de trésors différents il en faut au moins 12 et au plus 49
                nbTresorMax le nombre de trésors maximum distribué à chaque joueur
    résultat: le labyrinthe crée
    """
    nbJoueurs=len(nomsJoueurs)
    joueurs = ListeJoueurs(nomsJoueurs)
    plateau = Plateau(nbJoueurs, nbTresors)
    distribuerTresors(joueurs,nbTresors, nbTresorsMax)
    phase=1
    return [joueurs,plateau,phase]

laby = Labyrinthe(nomsJoueurs=["Julien","Etienne","Lucas"])

def getPlateau(labyrinthe):
    """
    retourne la matrice représentant le plateau de jeu
    paramètre: labyrinthe le labyrinthe considéré
    résultat: la matrice représentant le plateau de ce labyrinthe
    """
    return labyrinthe[1][0]



def getNbParticipants(labyrinthe):
    """
    retourne le nombre de joueurs engagés dans la partie
    paramètre: labyrinthe le labyrinthe considéré
    résultat: le nombre de joueurs de la partie
    """
    return getNbJoueurs(labyrinthe[0])


assert getNbParticipants(laby)==3

def getNomJoueurCourant(labyrinthe):
    """
    retourne le nom du joueur courant
    paramètre: labyrinthe le labyrinthe considéré
    résultat: le nom du joueurs courant
    """
    return nomJoueurCourant(labyrinthe[0])

assert getNomJoueurCourant(laby)=="Julien"


def getNumJoueurCourant(labyrinthe):
    """
    retourne le numero du joueur courant
    paramètre: labyrinthe le labyrinthe considéré
    résultat: le numero du joueurs courant
    """
    return numJoueurCourant(labyrinthe[0])

assert getNumJoueurCourant(laby)==1


def getPhase(labyrinthe):
    """
    retourne la phase du jeu courante
    paramètre: labyrinthe le labyrinthe considéré
    résultat: le numéro de la phase de jeu courante
    """
    return labyrinthe[2]

assert getPhase(laby)==1




def changerPhase(labyrinthe):
    """
    change de phase de jeu en passant la suivante
    paramètre: labyrinthe le labyrinthe considéré
    la fonction ne retourne rien mais modifie le labyrinthe
    """
    if labyrinthe[2]==1:
        labyrinthe[2]=2
    else:
        labyrinthe[2]=1

#changerPhase(laby)
#assert laby[2]==2



def getNbTresors(labyrinthe):
    """
    retourne le nombre de trésors qu'il reste sur le labyrinthe
    paramètre: labyrinthe le labyrinthe considéré
    résultat: le nombre de trésors sur le plateau
    """
    res=0
    for ligne in range(labyrinthe[1][0][0]):
        for colonne in range(labyrinthe[1][0][1]):
            if labyrinthe[1][0][2][ligne][colonne]["tresor"]!=0:
                res+=1
    return res


def getListeJoueurs(labyrinthe):
    """
    retourne la liste joueur structures qui gèrent les joueurs et leurs trésors
    paramètre: labyrinthe le labyrinthe considéré
    résultat: les joueurs sous la forme de la structure implémentée dans listeJoueurs.py
    """
    return labyrinthe[0]


def enleverTresor(labyrinthe,lin,col,numTresor):
    """
    enleve le trésor numTresor du plateau du labyrinthe.
    Si l'opération s'est bien passée le nombre total de trésors dans le labyrinthe
    est diminué de 1
    paramètres: labyrinthe: le labyrinthe considéré
                lig: la ligne où se trouve la carte
                col: la colonne où se trouve la carte
                numTresor: le numéro du trésor à prendre sur la carte
    la fonction ne retourne rien mais modifie le labyrinthe
    """
    matrice=getPlateau(labyrinthe)
    if matrice[2][lin][col]["tresor"]==numTresor:
        prendreTresor(matrice[2][lin][col])



def pionJoueur(persoPion, joueurs):
    """
    retourne le numéro de pion du joueur mis en paramétre
    """
    for i in range(len(joueurs[1])):
        if joueurs[1][i]["nom"] == persoPion:
            return i+1
    return None



def prendreJoueurCourant(labyrinthe,lin,col):
    """
    enlève le joueur courant de la carte qui se trouve sur la case lin,col du plateau
    si le joueur ne s'y trouve pas la fonction ne fait rien
    paramètres: labyrinthe: le labyrinthe considéré
                lig: la ligne où se trouve la carte
                col: la colonne où se trouve la carte
    la fonction ne retourne rien mais modifie le labyrinthe
    """
    joueurs = getListeJoueurs(labyrinthe)
    matrice=getPlateau(labyrinthe)
    joueurCourant = nomJoueurCourant(joueurs)
    pion = pionJoueur(joueurCourant,joueurs)
    #print(matrice[2][lin][col])
    prendrePion(matrice[2][lin][col], pion)
    #print(matrice[2][lin][col])



#prendreJoueurCourant(laby,0,0)
#carte = getPlateau(laby)[2][0][0]
#pion = pionJoueur(nomJoueurCourant(laby[0]),laby[0])
#assert not possedePion(carte,pion)


def pionJoueur(persoPion, joueurs):
    """
    retourne le numéro de pion du joueur mis en paramétre
    """
    for i in range(len(joueurs[1])):
        if joueurs[1][i]["nom"] == persoPion:
            return i+1
    return None



def poserJoueurCourant(labyrinthe,lin,col):
    """
    pose le joueur courant sur la case lin,col du plateau
    paramètres: labyrinthe: le labyrinthe considéré
                lig: la ligne où se trouve la carte
                col: la colonne où se trouve la carte
    la fonction ne retourne rien mais modifie le labyrinthe
    """
    joueurs = getListeJoueurs(labyrinthe)
    matrice=getPlateau(labyrinthe)
    joueurCourant = nomJoueurCourant(joueurs)
    pion = pionJoueur(joueurCourant,joueurs)
    for i in range(0,7):
        for j in range(0,7):
            prendrePion(matrice[2][i][j], pion)
    poserPion(matrice[2][lin][col], pion)


#poserJoueurCourant(laby,2,2)
#carte = getPlateau(laby)[2][2][2]
#pion = pionJoueur(nomJoueurCourant(laby[0]),laby[0])
#assert possedePion(carte,pion)



def getCarteAJouer(labyrinthe):
    """
    donne la carte à jouer
    paramètre: labyrinthe: le labyrinthe considéré
    résultat: la carte à jouer
    """
    return labyrinthe[1][1][0]

#print(getCarteAJouer(laby))



def coupInterdit(labyrinthe,direction,rangee):
    """
    retourne True si le coup proposé correspond au coup interdit
    elle retourne False sinon
    paramètres: labyrinthe: le labyrinthe considéré
                direction: un caractère qui indique la direction choisie ('N','S','E','O')
                rangee: le numéro de la ligne ou de la colonne choisie
    résultat: un booléen indiquant si le coup est interdit ou non
    """
    res = False
    if rangee%2 == 0:
        res = True
    elif direction == "N":
        if labyrinthe[1][1][1] == (0,rangee):
            res = True
    elif direction == "E":
        if labyrinthe[1][1][1] == (rangee,6):
            res = True
    elif direction == "S":
        if labyrinthe[1][1][1] == (6,rangee):
            res = True
    elif direction == "O":
        if labyrinthe[1][1][1] == (rangee,0):
            res = True
    return res

assert coupInterdit(laby,"N",0)
assert not coupInterdit(laby,"N",1)

def jouerCarte(labyrinthe,direction,rangee):
    """
    fonction qui joue la carte amovible dans la direction et sur la rangée passées
    en paramètres. Cette fonction
       - met à jour le plateau du labyrinthe
       - met à jour la carte à jouer
       - met à jour la nouvelle direction interdite
    paramètres: labyrinthe: le labyrinthe considéré
                direction: un caractère qui indique la direction choisie ('N','S','E','O')
                rangee: le numéro de la ligne ou de la colonne choisie
    Cette fonction ne retourne pas de résultat mais mais à jour le labyrinthe
    """
    if not coupInterdit(labyrinthe,direction,rangee):
        matrice = getPlateau(labyrinthe)
        amovible = getCarteAJouer(labyrinthe)
        if direction == "S":
            amovible = decalageColonneEnHaut(matrice,rangee,amovible)
            labyrinthe[1][1][0]=amovible
            labyrinthe[1][1][1]=(0,rangee)
            changerPhase(labyrinthe)
            if getListePions(getCarteAJouer(labyrinthe)) !=[]:
                setListePions(getVal(matrice,getNbLignes(matrice)-1,rangee),getListePions(getCarteAJouer(labyrinthe)))
                setListePions(getCarteAJouer(labyrinthe), [])
        if direction == "O":
            amovible = decalageLigneADroite(matrice,rangee,amovible)
            labyrinthe[1][1][0]=amovible
            labyrinthe[1][1][1]=(rangee,6)
            changerPhase(labyrinthe)
            if getListePions(getCarteAJouer(labyrinthe)) !=[]:
                setListePions(getVal(matrice,getNbLignes(matrice)-1,rangee),getListePions(getCarteAJouer(labyrinthe)))
                setListePions(getCarteAJouer(labyrinthe), [])
        if direction == "N":
            amovible = decalageColonneEnBas(matrice,rangee,amovible)
            labyrinthe[1][1][0]=amovible
            labyrinthe[1][1][1]=(6,rangee)
            changerPhase(labyrinthe)
            if getListePions(getCarteAJouer(labyrinthe)) !=[]:
                setListePions(getVal(matrice,getNbLignes(matrice)-1,rangee),getListePions(getCarteAJouer(labyrinthe)))
                setListePions(getCarteAJouer(labyrinthe), [])
        if direction == "E":
            amovible = decalageLigneAGauche(matrice,rangee,amovible)
            labyrinthe[1][1][0]=amovible
            labyrinthe[1][1][1]=(rangee,0)
            changerPhase(labyrinthe)
            if getListePions(getCarteAJouer(labyrinthe)) !=[]:
                setListePions(getVal(matrice,getNbLignes(matrice)-1,rangee),getListePions(getCarteAJouer(labyrinthe)))
                setListePions(getCarteAJouer(labyrinthe), [])


#jouerCarte(laby,"N",1)



def tournerCarte(labyrinthe,sens='H'):
    """
    tourne la carte à jouer dans le sens indiqué en paramètre (H horaire A antihoraire)
    paramètres: labyritnthe: le labyrinthe considéré
                sens: un caractère indiquant le sens dans lequel tourner la carte
     Cette fonction ne retourne pas de résultat mais mais à jour le labyrinthe
    """
    amovible = getCarteAJouer(labyrinthe)
    if sens == "H":
        tournerHoraire(amovible)
        labyrinthe[1][1][0]=amovible
    else:
        tournerAntiHoraire(amovible)
        labyrinthe[1][1][0]=amovible


#tournerCarte(laby)

def getTresorCourant(labyrinthe):
    """
    retourne le numéro du trésor que doit cherche le joueur courant
    paramètre: labyritnthe: le labyrinthe considéré
    resultat: le numéro du trésor recherché par le joueur courant
    """
    joueurs = getListeJoueurs(labyrinthe)
    return tresorCourant(joueurs)




def getCoordonneesTresorCourant(labyrinthe):
    """
    donne les coordonnées du trésor que le joueur courant doit trouver
    paramètre: labyritnthe: le labyrinthe considéré
    resultat: les coordonnées du trésor à chercher ou None si celui-ci
              n'est pas sur le plateau
    """
    joueurs = getListeJoueurs(labyrinthe)
    matrice = getPlateau(labyrinthe)
    tresor = getTresorCourant(labyrinthe)
    for i in range(0,7):
        for j in range(0,7):
            if getTresor(matrice[2][i][j]) == tresor:
                return(i,j)
    return None

#getCoordonneesTresorCourant(laby)

def getCoordonneesJoueurCourant(labyrinthe):
    """
    donne les coordonnées du joueur courant sur le plateau
    paramètre: labyritnthe: le labyrinthe considéré
    resultat: les coordonnées du joueur courant ou None si celui-ci
              n'est pas sur le plateau
    """
    joueurs = getListeJoueurs(labyrinthe)
    matrice = getPlateau(labyrinthe)
    joueurCourant = nomJoueurCourant(joueurs)
    pion = pionJoueur(joueurCourant,joueurs)
    for i in range(0,7):
        for j in range(0,7):
            if possedePion(matrice[2][i][j],pion):
                return(i,j)
    return None

#getCoordonneesJoueurCourant(laby)

def executerActionPhase1(labyrinthe,action,rangee):
    """
    exécute une action de jeu de la phase 1
    paramètres: labyrinthe: le labyrinthe considéré
                action: un caractère indiquant l'action à effecter
                        si action vaut 'T' => faire tourner la carte à jouer
                        si action est une des lettres N E S O et rangee est un des chiffre 1,3,5
                        => insèrer la carte à jouer à la direction action sur la rangée rangee
                           et faire le nécessaire pour passer en phase 2
    résultat: un entier qui vaut
              0 si l'action demandée était valide et demandait de tourner la carte
              1 si l'action demandée était valide et demandait d'insérer la carte
              2 si l'action est interdite car l'opposée de l'action précédente
              3 si action et rangee sont des entiers positifs
              4 dans tous les autres cas
    """
    if action == "T":
        tournerCarte(labyrinthe)
        return 0
    elif action=="N" or action=="E" or action=="S" or action=="O":
        jouerCarte(labyrinthe,action,rangee)
        if labyrinthe[2]==1:#vérifie a quelle phase on est (si on est en phase 2 cela signifi que l'action c'est bien passé)
            return 1
        else:
            return 2
    elif type(action)==int and type(rangee)==int:
        return 3
    return 4


#print(executerActionPhase1(laby,"N",0))


def accessibleDistJoueurCourant(labyrinthe,ligA,colA):
    """
    verifie si le joueur courant peut accéder la case ligA,colA
    si c'est le cas la fonction retourne une liste représentant un chemin possible
    sinon ce n'est pas le cas, la fonction retourne None
    paramètres: labyrinthe le labyrinthe considéré
                ligA la ligne de la case d'arrivée
                colA la colonne de la case d'arrivée
    résultat: une liste de couples d'entier représentant un chemin que le joueur
              courant atteigne la case d'arrivée s'il existe None si pas de chemin
    """
    #print(labyrinthe)
    res = None
    plateau = labyrinthe[1]
    coordonne = getCoordonneesJoueurCourant(labyrinthe)
    if coordonne != None:
        ligD = coordonne[0]
        colD = coordonne[1]
        res = accessibleDist(plateau[0],ligD,colD,ligA,colA)
    return res

#accessibleDistJoueurCourant(laby,1,1)

def finirTour(labyrinthe):
    """
    vérifie si le joueur courant vient de trouver un trésor (si oui fait le nécessaire)
    vérifie si la partie est terminée, si ce n'est pas le cas passe au joueur suivant
    paramètre: labyrinthe le labyrinthe considéré
    résultat: un entier qui vaut
              0 si le joueur courant n'a pas trouvé de trésor
              1 si le joueur courant a trouvé un trésor mais la partie n'est pas terminée
              2 si le joueur courant a trouvé son dernier trésor (la partie est donc terminée)
    """
    joueurs = getListeJoueurs(labyrinthe)
    plateau = getPlateau(labyrinthe)
    numJoueur = getNumJoueurCourant(labyrinthe)
    coordonneJoueur = getCoordonneesJoueur(plateau,numJoueur)
    ligne = coordonneJoueur[0]
    colonne = coordonneJoueur[1]
    coordonneTresor = getCoordonneesTresorCourant(labyrinthe)
    if coordonneTresor == coordonneJoueur:
        tresor = prendreTresor(plateau[2][ligne][colonne])
        #enleverTresor(labyrinthe,lin,col,numTresor) ?
        joueurCourantTrouveTresor(joueurs)
        #print(joueurCourantAFini(joueurs))
        if joueurCourantAFini(joueurs):
            return 2
        else:
            changerJoueurCourant(joueurs)
            changerPhase(labyrinthe)
            return 1
    else:
        changerJoueurCourant(joueurs)
        changerPhase(labyrinthe)
        return 0

#print(finirTour(laby))
