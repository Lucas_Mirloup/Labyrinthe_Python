def Joueur(nom):
    """
    creer un nouveau joueur portant le nom passé en paramètre. Ce joueur possède une liste de trésors à trouver vide
    paramètre: nom une chaine de caractères
    retourne le joueur ainsi créé
    """
    return {"nom":nom,"TresorATrouver":[]}

joueur={"nom":"Julien","TresorATrouver":[]}
assert joueur=={"nom":"Julien","TresorATrouver":[]}


def ajouterTresor(joueur,tresor):
    """
    ajoute un trésor à trouver à un joueur (ce trésor sera ajouté en fin de liste) Si le trésor est déjà dans la liste des trésors à trouver la fonction ne fait rien
    paramètres:
        joueur le joueur à modifier
        tresor un entier strictement positif
    la fonction ne retourne rien mais modifie le joueur
    """
    if tresor not in joueur["TresorATrouver"]:
        joueur["TresorATrouver"].append(tresor)

ajouterTresor(joueur,5)
ajouterTresor(joueur,12)
assert joueur=={"nom":"Julien","TresorATrouver":[5,12]}


def prochainTresor(joueur):
    """
    retourne le prochain trésor à trouver d'un joueur, retourne None si aucun trésor n'est à trouver
    paramètre:
        joueur le joueur
    résultat un entier représentant lJoueur("julien")e trésor ou None
    """
    if len(joueur["TresorATrouver"])==0:
        return None
    else:
        return joueur["TresorATrouver"][0]

assert prochainTresor(joueur)==5


def tresorTrouve(joueur):
    """
    enleve le premier trésor à trouver car le joueur l'a trouvé
    paramètre:
        joueur le joueur
    la fonction ne retourne rien mais modifie le joueur
    """
    del joueur["TresorATrouver"][0]

tresorTrouve(joueur)
assert joueur=={"nom":"Julien","TresorATrouver":[12]}


def getNbTresorsRestants(joueur):
    """
    retourne le nombre de trésors qui reste à trouver
    paramètre: joueur le joueur
    résultat: le nombre de trésors attribués au joueur
    """
    return len(joueur["TresorATrouver"])

assert getNbTresorsRestants(joueur)==1

def getNom(joueur):
    """
    retourne le nom du joueur
    paramètre: joueur le joueur
    résultat: le nom du joueur
    """
    return joueur["nom"]

assert getNom(joueur)=="Julien"
